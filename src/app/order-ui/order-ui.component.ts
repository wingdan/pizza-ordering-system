import { Component, OnInit } from '@angular/core';
import {OrderServiceService} from '../_services/order-service.service';
import {Pizza} from '../../classes/Pizza';
import { Menu, PizzaSize } from 'src/classes/Menu';
import { Topping } from 'src/classes/Topping';
import { Offer } from 'src/classes/Offer';
import { Cart } from 'src/classes/Cart';


@Component({
  selector: 'app-order-ui',
  templateUrl: './order-ui.component.html',
  styleUrls: ['./order-ui.component.css']
})



export class OrderUIComponent implements OnInit {
  ToppingOptions: any[];
  OfferOptions: any[];
  TempToppings: Set<Topping>;
  TempPizzaSize: PizzaSize;
  MyPizzas: Pizza[];
  Menu: Menu;
 



  constructor(private orderServiceService : OrderServiceService) { 

  }

  ngOnInit() {
    this.TempToppings = new Set<Topping>();
    this.getMenu();
    this.getPizzas();
    this.getOffers();
    this.ToppingOptions = this.Menu.Toppings.map<any>(topping => { return { Topping:topping, checked:false} });
  }

  getPizzas():void {
    this.MyPizzas = this.orderServiceService.getPizzas();
  }

  getMenu():void {
    this.Menu = this.orderServiceService.getMenu();
  }

  getOffers(): void {
    let offers = this.orderServiceService.getOffers();
    this.OfferOptions = offers.map<any>(offer=> { return {Offer:offer, disabled:true, selected:false} })
  }


  getPizzaPrice(myPizza: Pizza):number {
    let toppingPrice = 0;
    myPizza.Toppings.forEach(x=> toppingPrice+=x.Price);
    let price = myPizza.PizzaSize.Price +toppingPrice;

    if(myPizza.Offer!=undefined) {
      if(myPizza.Offer.OfferType==0) {
        price = myPizza.Offer.OfferRate;
  
      }
      else if(myPizza.Offer.OfferType==1) {
        price = myPizza.Offer.OfferRate * price;
      }
    }
    return price;
  }

  onPizzaSizeChange(myPizzaSize: PizzaSize) {

    this.TempPizzaSize = myPizzaSize;
  }


  onToppingChange(myTopping: Topping, event:any) {

    if(event.target.checked) {
      this.TempToppings.add(myTopping);
    }
    else {
      this.TempToppings.delete(myTopping);
    }
    
  }

  onAddPizza():void {
    
    if(this.TempPizzaSize!=null && this.TempToppings.size!=0) {
      this.MyPizzas.push({PizzaSize:this.TempPizzaSize, Toppings: new Set(this.TempToppings), Offer:undefined});
      this.TempPizzaSize = null;
      this.TempToppings.clear();
      this.ToppingOptions.map(x=>x.checked=false);
      this.checkOffer(this.MyPizzas);

    }
  }

  removePizza(myPizza: Pizza) {
    if(this.MyPizzas.indexOf(myPizza) > -1) {
      this.MyPizzas.splice(this.MyPizzas.indexOf(myPizza), 1);
      this.removeAllPizzaOffer() ;
      this.checkOffer(this.MyPizzas);


    }
  }

  checkOffer(Pizzas:Pizza[]) : void {
    this.orderServiceService.checkOffers(Pizzas).map((x,i)=> {
      if(x) {
        this.OfferOptions[i].disabled = null;
      }
      else {
        this.OfferOptions[i].disabled = true;
      }
    })
  }

  removeAllPizzaOffer() :void {
    this.MyPizzas.forEach(x=>x.Offer=undefined);
  }

  addPizzaOffer(offerOption: any) :void {
    
    if(offerOption.Offer.OfferID=="1" || offerOption.Offer.OfferID=="3" ) {
      console.log('offerOption', offerOption);
      this.MyPizzas.filter(x=> x.PizzaSize.Size==offerOption.Offer.PizzaSize)[0].Offer = offerOption.Offer;
    }
    else if(offerOption.Offer.OfferID=="2") {
      this.MyPizzas.filter(x=> x.PizzaSize.Size==offerOption.Offer.PizzaSize)[0].Offer = offerOption.Offer;
      this.MyPizzas.filter(x=> x.PizzaSize.Size==offerOption.Offer.PizzaSize)[1].Offer = offerOption.Offer;
    }

  }



  onOfferSelectionChange(offerOption: any, $event:any) :void {
    this.OfferOptions.forEach(x=> x.selected=false);
    offerOption.selected = $event.target.checked;
    this.removeAllPizzaOffer();
    console.log('offerOption', offerOption);
    this.addPizzaOffer(offerOption);

  }


}
