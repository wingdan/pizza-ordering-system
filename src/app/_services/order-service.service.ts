import { Injectable } from '@angular/core';
import { Pizza } from '../../classes/Pizza';
import { Topping } from '../../classes/Topping';
import { Menu, PizzaSize } from '../../classes/Menu';
import { Offer } from 'src/classes/Offer';

@Injectable({
  providedIn: 'root'
})
export class OrderServiceService {
  private Pizzas: Pizza[];
  private Menu: Menu;
  private Offers: Offer[];

  constructor() { 
    this.Pizzas = new Array<Pizza>();
    this.Menu = this.initMenu();
    this.Offers = this.initOffers();
  }

  addPizza(NewPizza: Pizza): void {
    console.log(NewPizza);
    this.Pizzas.push(NewPizza);
  }

  getPizzas(): Pizza[] {
    return this.Pizzas;
  }

  private initMenu(): Menu {
    let pizzaSizes: PizzaSize[];
    let toppings: Topping[];

    pizzaSizes = [{Size:"Small", Price:5}, {Size:"Medium", Price:7}, {Size:"Large", Price:8},  {Size:"Extra Large", Price:9}]
    toppings = [{Name:"Tomatoes", Price: 1.00, IsVeg:true, Count:1}, 
      {Name:"Onions", Price:0.50, IsVeg:true, Count:1},
      {Name:"Bell pepper", Price:1.00, IsVeg:true, Count:1},
      {Name:"Mushrooms", Price:1.20, IsVeg:true, Count:1},
      {Name:"Pineapple", Price:0.75, IsVeg:true, Count:1},
      {Name:"Sausage ", Price:1.00, IsVeg:false, Count:1},
      {Name:"Pepperoni ", Price:2.00,IsVeg:false, Count:2},
      {Name:"Barbecue chicken", Price:3.00, IsVeg:false, Count:2}];

    return {PizzaSizes:pizzaSizes, Toppings:toppings};
  }

  private initOffers(): Offer[] {
    let offers: Offer[];

    offers = [{OfferID:"1", PizzaSize:"Medium", PizzaCount:1, ToppingCount:2, OfferType:0, OfferRate:5}, 
      {OfferID:"2", PizzaSize:"Medium", PizzaCount:2, ToppingCount:4, OfferType:0, OfferRate:9},
      {OfferID:"3", PizzaSize:"Large", PizzaCount:1, ToppingCount:4, OfferType:1, OfferRate:0.5}];

    return offers;
  }

  public getMenu(): Menu {
    return this.Menu;
  }

  public getOffers(): Offer[] {
    return this.Offers;
  }

  public checkOffers(Pizzas:Pizza[]):boolean[] {
    let offerStatuses = [];
    
    this.Offers.map((offer,i) => {
        offerStatuses[i] = false; 
        let myPizzas = Pizzas.filter(pizza=> pizza.PizzaSize.Size==offer.PizzaSize);

        let isToppingCountValid = myPizzas.map(myPizza => { 
          let tCount = 0;
          myPizza.Toppings.forEach(t=> {
            tCount+=t.Count; 
          });
          return tCount >= offer.ToppingCount;
        });
      
        if(isToppingCountValid.filter(x=>x==true).length>=offer.PizzaCount) {
          offerStatuses[i] = true; 
        }
    });
    return offerStatuses;
  }
}
