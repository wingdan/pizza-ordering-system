export interface Topping {
    Name: string;
    Price: number;
    IsVeg: Boolean;
    Count: number;
}