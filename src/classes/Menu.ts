import {Topping} from './Topping';

export interface PizzaSize {
    Size: string;
    Price: number;
}

export interface Menu {
    PizzaSizes: PizzaSize[];
    Toppings: Topping[];
}