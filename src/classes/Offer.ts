export interface Offer {
    OfferID: string;
    PizzaSize: string;
    PizzaCount: number;
    ToppingCount: number;
    OfferType: number;      // 0=flat rate, 1=discount rate
    OfferRate: number;
}