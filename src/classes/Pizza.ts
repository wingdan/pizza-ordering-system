import {Topping} from './Topping';
import {Menu, PizzaSize} from './Menu';
import { Offer } from './Offer';

export class Pizza {
    PizzaSize: PizzaSize;
    Toppings: Set<Topping>;
    Offer: Offer;   // should not belong to here. make it up for quick implementation

    constructor() {
        this.Toppings = new Set<Topping>();
    }
}