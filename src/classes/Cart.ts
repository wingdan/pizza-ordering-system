import { Offer } from './Offer';
import { Pizza } from './Pizza';

export interface CartItem {
    Item: Pizza;
    Offer: Offer;
}

export interface Cart {
    CartItems: CartItem[];
}